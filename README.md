# FrontEnd

Este projeto trata-se do código da aplicação de FrontEnd (HTML, CSS, JavaScript) implementada para a disciplina de Cloud Computing & DevSecOps do curso de pós graduação em engenharia Devops da Universidade Positivo em Curitiba/PR.

## Estrutura do projeto

* src - Diretório com código fonte da Aplicação

* terraform - Diretório com a automação de criação dos pré-requisitos na cloud para comportar uma aplicação estática de FrontEnd

* bitbucket-pipelines.yml - Arquivo de configuração do pipeline

## Infra-Estrutura Cloud

Este projeto trata-se de uma aplicação de FrontEnd básica, com arquivos estáticos (html, css e JavaScript)

Ele é provisionada no Google Cloud Platform, utilizando o recurso de **Google Cloud Storage**, **Google Cloud Load Balancing** e **Google Cloud DNS** para disponibilização da aplicação.

## Pipeline

Estamos utilizando o Bitbucket Pipelines para provisionamento dos recursos de Cloud necessários, bem como o deploy do código da aplicação.

O provisionamento de componentes de infraestrutura está sendo feito via Terraform.

### Segurança

No processo de Pull Request e antes de cada solicitação de deploy para o ambiente executamos a análise estática de código através do Sonar Cloud. Só é permitido o merge do Pull Request, caso não seja relatado nenhuma infração através do Sonar.

Exemplo do retorno obtido no Pull Request:

Verificar exemplo no PR: https://bitbucket.org/geovanaSouza/frontend/pull-requests/21

![Sonar](SonarPRFront.png)


### Deploy do ambiente

Mantemos o ambiente down por questões de economia $$$

Para efetuar o provisionamento e deploy da aplicação, seguir os seguintes passos:

* Acessar o menu pipelines: https://bitbucket.org/geovanaSouza/frontend/addon/pipelines/home#!/

* Botão "Run pipeline" na tela superior direita

* Escolher branch: master e custom: deploy

![RunPipeline](RunPipelineDeploy.png)

* Pressionar o botão Run

* Primeiramente serão provisionados os recursos de Cloud Necessários para o ambiente (Buckets, LoadBalance e entrada DNS).

* Posteriormente será feito o deploy da aplicação na etapa "Deploy FrontEnd"

* Após finalizado o pipeline, abrir o browser e acessar a URL http://up.geovanapossenti.com/ Pode demorar alguns minutos (~10minutos) até que o endereço seja propagado pela Internet.

### Destroy ambiente

Para fins de economia é sempre aconselhável remover o ambiente após o uso. Para isto, existe um step no pipeline chamado "Destroy"

* Acessar o menu pipelines: https://bitbucket.org/geovanaSouza/frontend/addon/pipelines/home#!/

* Botão "Run pipeline" na tela superior direita

* Escolher branch: master e custom: destroy

* Pressionar o botão Run

* Todos os componentes serão removidos da cloud

### Pré-Requisitos Pipeline

Em Deployments (https://bitbucket.org/geovanaSouza/frontend/admin/addon/admin/pipelines/deployment-settings) existem as configurações de variáveis de ambiente necessárias para execução de cada um dos steps do pipeline.

#### Deployment prod-infrastructure

Definição de variáveis necessárias para execução do TerraForm. São elas:

* GOOGLE_CREDENTIALS - Credencial de cloud com permissão de provisionamento dos recursos que são pré-requisitos para o deploy da aplicação.

* STATE_TF_BUCKET - Nome do bucket onde será armazenado o arquivo de estado do TerraForm

* TF_VAR_FILE - Nome do arquivo que contém variáveis de ambiente necessárias para execução do TerraForm

#### Deployment prod-application

Definição de variáveis necessárias para execução do deploy do código fonte da aplicação. São elas:

* GCP_PROJECT - Projeto do Google Cloud Platform em que a aplicação será "deployada"

* BUCKET_NAME - Nome do bucket para onde os arquivos da Aplicação de FrontEnd serão armazenados na Cloud

* KEY_FILE - Credencial de cloud com permissão para Deploy da aplicação

## Fluxo de desenvolvimento

* Crie uma feature branch à partir da master

* Efetue a alteração desejada

* Criar um Pull Request: origem: feature destino: master

* Obtenha uma aprovação do Default Reviewer. Atualmente Geovana e Victor são os aprovadores de modificações neste repositório. Este time recebe automaticamente e-mails de quando um Pull Request é efetuado para este repositório. Não é necessário notificá-los.

* Efetue o Merge do Pull Request.
