variable "GCP_PROJECT" {}
variable "STATE_TF_BUCKET" {}
variable "APPLICATION_BUCKET" {}
variable "DOMAIN" {}

provider "google" {
  project = var.GCP_PROJECT
  region = "us-central1"
}

//Shared state
terraform {
  backend "gcs" {
    prefix = "terraform/state"
  }
}

resource "google_storage_bucket" "static-site" {
  name          = var.APPLICATION_BUCKET
  location      = "US-CENTRAL1"
  force_destroy = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}

resource "google_compute_backend_bucket" "static-site-backend-bucket" {
  name        = "backend-bucket"
  description = "Backend Load Balance Aplicação FrontEnd"
  bucket_name = google_storage_bucket.static-site.name
}

resource "google_compute_url_map" "static-site-urlmap" {
  name            = "frontend-lb"
  default_service = "${google_compute_backend_bucket.static-site-backend-bucket.self_link}"
}

resource "google_compute_target_http_proxy" "static-site-http-proxy" {
  name        = "static-site-http-proxy"
  description = "HTTP proxy"
  url_map     = "${google_compute_url_map.static-site-urlmap.self_link}"
}

resource "google_compute_global_forwarding_rule" "fw-lb" {
  name = "fw-lb"
  target = "${google_compute_target_http_proxy.static-site-http-proxy.self_link}"
  port_range = 80
}

resource "google_dns_record_set" "a" {
  project = "posup2020"
  name         = "${var.DOMAIN}.geovanapossenti.com."
  managed_zone = "geovanapossenti-com"
  type         = "A"
  ttl          = 0

  rrdatas = [google_compute_global_forwarding_rule.fw-lb.ip_address]
}

output "external_ip" {
  description = "IP do LoadBalance criado"
  value       = google_compute_global_forwarding_rule.fw-lb.ip_address
}

