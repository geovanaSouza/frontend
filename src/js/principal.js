var tituloPrincipal = document.querySelector(".titulo-principal");
var tituloAplicacao = document.querySelector(".tituloAplicacao");
tituloPrincipal.textContent = "Cloud Computing & DevSecOps";
tituloAplicacao.textContent = "Application";

var alunos = document.querySelectorAll(".aluno");

for (var i = 0; i < alunos.length; i++) {
  aluno = alunos[i];

  var nota1 = aluno.querySelector(".nota1").textContent;
  var nota2 = aluno.querySelector(".nota2").textContent;
  var media = aluno.querySelector(".media");

  var nota1EhValida = true;
  var nota2EhValida = true;

  if(nota1 < 0 || nota1 > 10){
    console.log("Nota1 inválida!");
    nota1EhValida = false;
    media.textContent = "Nota1 inválida!";
  }

  if(nota2 < 0 || nota2 > 10){
    console.log("Nota2 inválida!");
    nota2EhValida = false;
    media.textContent = "Nota2 inválida!";
  }

  if (nota1EhValida && nota2EhValida){
    media.textContent = ((parseInt(nota1) + parseInt(nota2))/2);
  }

}


console.log("Buscando alunos...");

var xhr = new XMLHttpRequest();
xhr.open("GET", "https://us-central1-posup2020.cloudfunctions.net/Alunos");
xhr.addEventListener("load", function(){
  var resposta = xhr.responseText;

  var alunos = JSON.parse(resposta);

  alunos.forEach( function(aluno) {
    adicionaAlunoNaTabela(aluno);
  });



});

xhr.send();

function adicionaAlunoNaTabela(aluno) {
  var alunoTr = montaTr(aluno);
  var tabela = document.querySelector("#tabela-alunos");
  tabela.appendChild(alunoTr);
}

function montaTr(aluno) {
    var alunoTr = document.createElement("tr");
    alunoTr.classList.add("aluno");

    alunoTr.appendChild(montaTd(aluno.nome, "info-nome"));
    alunoTr.appendChild(montaTd(aluno.nota1, "info-nota1"));
    alunoTr.appendChild(montaTd(aluno.nota2, "info-nota2"));

    return alunoTr;
}

function montaTd(dado, classe) {
    var td = document.createElement("td");
    td.classList.add(classe);
    td.textContent = dado;

    return td;
}
